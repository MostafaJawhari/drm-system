﻿using DRMSystem.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace DRMSystem.Data.SQL
{
    public class RolesDataManager
    {
        ApplicationDbContext context = new ApplicationDbContext();
        public bool AddRole(Role role)
        {
            context.Roles.Add(new Microsoft.AspNet.Identity.EntityFramework.IdentityRole()
            {
                Name = role.RoleName
            });

            context.SaveChanges();

            return true;
        }
    }
}
