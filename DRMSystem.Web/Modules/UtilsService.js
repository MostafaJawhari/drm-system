﻿

app.service('UtilsService', ['$q', function ($q) {

    "use strict";

    function getServiceURL( controllerName, actionName) {
        return '/api/' + controllerName + '/' + actionName;
    }

    return ({
        getServiceURL: getServiceURL,
    });

}]);