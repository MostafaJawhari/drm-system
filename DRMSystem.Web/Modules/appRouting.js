﻿/// <reference path="../Views/Home/Index.cshtml" />
var appRouting = angular.module('appRouting', ['ngRoute']);
appRouting.config(['$routeProvider',
  function ($routeProvider) {
      $routeProvider.
        when('/', {
            templateUrl: '/Pages/FirstPage.html'
        }).
        when('/Roles', {
            templateUrl: '/Pages/Roles/Roles.html'
        }).
        when('/AddRole', {
            templateUrl: '/Pages/Roles/RolesEditor.html'
        }).
        otherwise({
            redirectTo: '/'
        });
  }]);