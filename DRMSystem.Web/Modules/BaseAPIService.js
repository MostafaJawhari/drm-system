﻿app.service('BaseAPIService', function ($http, $q, $location, $rootScope) {

    return ({
        get: get,
        post: post
    });

    function get(url, params) {
        var deferred = $q.defer();

        var urlParameters;
        if (params)
            urlParameters = {
                params: params,
            };

        $http.get(url, urlParameters)
            .success(function (response, status, headers, config) {
                deferred.resolve(response);
            })
            .error(function (data, status, headers, config) {

                console.log('');
                console.log('Error Occured: ' + data.ExceptionMessage);
                console.log('');
                console.log(data);
                deferred.reject(data);
            });
        return deferred.promise;
    }

    function post(url, dataToSend) {
        var deferred = $q.defer();
        var data;
        if (dataToSend)
            data = dataToSend
        var responseType = '';
        var ContentType = 'application/json;charset=utf-8';

        var req = {
            method: 'POST',
            url: url,
            responseType: responseType,
            headers: {
                'Content-Type': ContentType
            },
            data: data
        }
        $http(req)
            .success(function (response, status, headers, config) {
                deferred.resolve(response);
            })
            .error(function (data, status, headers, config) {

                console.log('');
                console.log('Error Occured: ' + data.ExceptionMessage);
                console.log('');
                console.log(data);
                deferred.reject(data);
            });
        return deferred.promise;

    }

});