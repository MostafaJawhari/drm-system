﻿using DRMSystem.Business;
using DRMSystem.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DRMSystem.Web.Controllers
{
    [RoutePrefix("api/Roles")]
    public class RolesController : ApiController
    {

        [HttpPost]
        [Route("AddRole")]
        public void AddRole(Role role)
        {
            RolesManager manager = new RolesManager();
            manager.AddRole(role);
        }

    }
}
