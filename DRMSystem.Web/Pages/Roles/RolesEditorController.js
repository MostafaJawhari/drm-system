﻿(function (appControllers) {

    "use strict";

    rolesEditorController.$inject = ['$scope', 'DRMSystem_RolesAPIService'];

    function rolesEditorController($scope, DRMSystem_RolesAPIService) {
        var x = $scope.roleName;

        $scope.save = function () {
            DRMSystem_RolesAPIService.AddRole({ RoleName: $scope.roleName });
        };
    
    }

    appControllers.controller('DRMSystem_RolesEditorController', rolesEditorController);
})(appControllers);
