﻿(function (appControllers) {

    "use strict";
    rolesAPIService.$inject = ['BaseAPIService', 'UtilsService'];

    function rolesAPIService(BaseAPIService, UtilsService) {

        var controllerName = 'Roles';

        
        function AddRole(roleObject) {
            return BaseAPIService.post(UtilsService.getServiceURL(controllerName, "AddRole"), roleObject);
        }

        return ({
            AddRole: AddRole
        });
    }

    appControllers.service('DRMSystem_RolesAPIService', rolesAPIService);

})(appControllers);