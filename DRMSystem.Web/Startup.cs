﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DRMSystem.Web.Startup))]
namespace DRMSystem.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
