﻿using System.Web;
using System.Web.Optimization;

namespace DRMSystem.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            BundleTable.EnableOptimizations = false;
            bundles.DirectoryFilter.Clear();
            bundles.IgnoreList.Clear();


            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));


            #region JQuery
            bundles.Add(new ScriptBundle("~/bundles/JQuery").Include(
                "~/Libraries/Bower/jquery/dist/jquery.min.js"));
            #endregion

            #region Angular

            bundles.Add(new ScriptBundle("~/bundles/Angular").Include(
               "~/Libraries/Bower/angular/angular.js",
               "~/Libraries/Bower/angular-route/angular-route.js"
              ));

            #endregion





            #region Bootstrap

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").IncludeDirectory(
                "~/Libraries/Bootstrap", "*.js", false));

            #endregion


            bundles.Add(new StyleBundle("~/Content/css").IncludeDirectory(
                      "~/Content/", "*.css", false));

            bundles.Add(new StyleBundle("~/Content/slickGrid/css").IncludeDirectory(
                    "~/Content/slickGrid/", "*.css", false));

            bundles.Add(new StyleBundle("~/Content/themes/base/css").IncludeDirectory(
            "~/Content/themes/base/", "*.css", false));
        
        



            //Style bootstrap
            bundles.Add(new StyleBundle("~/Content/bootstrap").IncludeDirectory(
               "~/Libraries/Bootstrap", "*.css", true).IncludeDirectory(
               "~/Libraries/Bootstrap", "*.png", true));





            //Site
            bundles.Add(new ScriptBundle("~/bundles/Javascripts").IncludeDirectory(
                "~/Modules", "*.js", true));

            //AngularAPIServices
            bundles.Add(new ScriptBundle("~/bundles/AngularAPIServices").IncludeDirectory(
                "~/APIServices", "*.js", true));

            //AngularControllers
            bundles.Add(new ScriptBundle("~/bundles/AngularControllers").IncludeDirectory(
                "~/Pages/Roles", "*.js", true));

            //Slick Grid
            bundles.Add(new ScriptBundle("~/bundles/SlickGrid").IncludeDirectory(
                "~/Libraries/SlickGrid", "*.js", true));




        }
    }
}
